# TP1 INF3005

Ce projet est réalisé dans le cadre du cours INF3005 - Programmation Web Avancée à la session d'hiver 2018.

Il s'agit d'un logiciel de feuille de temps en ligne. Ce logiciel permet aux employés d'entrer leurs heures travaillées et de produire la facturation en fonction de ces heures. Il permet aussi de voir les détails des heures travaillées par jour et un résumé des heures par mois.

# Dépendances

 - [Flask](http://flask.pocoo.org/)
 - [WTForms](http://wtforms.readthedocs.io/en/latest/)
 - [Dateutil](https://dateutil.readthedocs.io/en/stable/)

# Installation

Pour installer les dépendances du projet, il suffit de lancer la cible 'install' du [Makefile](Makefile) avec la commande suivante:

`make install`

Il est aussi possible de le faire directement avec pip avec la commande suivante:

`pip3 install --user -r requirements.txt`

# Exécution

Lancer la cible 'run' du [Makefile](Makefile) avec la commande:

`make run`

Ceci lancera le serveur de développement de Flask sur le port 5000.

# Auteur

Alexandre Côté Cyr - COTA07049302

# License

Ce project est sous licence [GPL3](LICENSE.md).
