#!/usr/bin/env python
# -*- encoding: utf-8 -*-

from wtforms import Form
from wtforms import StringField
from wtforms import validators
from wtforms import DateField
from wtforms import FloatField
from wtforms import HiddenField

ERROR_REQUIRED = "Ce champ doit être rempli."
ERROR_HOURS = "Le nombre d'heures doit être entre 0 et 24."
ERROR_MATRICULE = "Le matricule doit être dans le format XXX-99."
ERROR_LENGTH = "Le code de projet doit être entre 2 et 15 caractères."
MATRICULE_REGEX = "^[A-Z]{3}-\d{2}$"


class MatriculeForm(Form):
    matricule = StringField("Numéro de matricule", [
                           validators.DataRequired(message=ERROR_REQUIRED),
                           validators.Regexp(MATRICULE_REGEX,
                                             message=ERROR_MATRICULE)
    ])


class AddTimeForm(Form):
    project = StringField("Identificateur de Projet", [
                         validators.DataRequired(message=ERROR_REQUIRED),
                         validators.Length(min=2, max=15,
                                           message=ERROR_LENGTH)
    ])

    hours = FloatField("Nombre d'heures travaillées", [
                      validators.DataRequired(),
                      validators.NumberRange(min=0, max=24,
                                             message=ERROR_HOURS)
    ])
