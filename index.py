#!/usr/bin/env python
# -*- encoding: utf-8 -*-

from flask import Flask
from flask import render_template
from flask import redirect
from flask import request
from flask import g
from flask import url_for

from datetime import date

from .database import Database
from .forms import *
from .helpers import *

app = Flask(__name__, static_url_path="/static", static_folder="static")


def get_db():
    db = getattr(g, '_database', None)
    if db is None:
        g._database = Database()
    return g._database


@app.teardown_appcontext
def close_connection(exception):
    db = getattr(g, '_database', None)
    if db is not None:
        db.disconnect()


@app.route("/", methods=["GET", "POST"])
def home_page():
    form = MatriculeForm(request.form)
    if request.method == "POST" and form.validate():
        return redirect(url_for("time_for_day",
                                matricule=form.matricule.data,
                                date_du_jour=date.today().isoformat()))
    return render_template("index.html", form=form)


@app.route("/<matricule>/<date_du_jour>", methods=["GET", "POST"])
def time_for_day(matricule, date_du_jour):
    form = AddTimeForm(request.form)
    if request.method == "POST" and form.validate():
        get_db().upsert_project_hours(matricule,
                                      form.project.data,
                                      date_du_jour,
                                      form.hours.data)
        return redirect(request.referrer, "302")
    hours = get_db().get_hours_for_day(matricule, date_du_jour)
    dates = get_prev_next_day(date_du_jour)
    return render_template("time_for_day.html",
                           matricule=matricule,
                           date=dates,
                           hours=hours,
                           form=form)


@app.route('/<matricule>/overview/<mois>')
def overview_month(matricule, mois):
    month_info = get_db().get_monthly_summary(matricule, mois)
    month_info.update(get_prev_next_month(mois))
    return render_template("summary_for_month.html",
                           matricule=matricule,
                           month_info=month_info)


@app.route("/<matricule>")
def overview_global(matricule):
    global_summary = get_db().get_general_summary(matricule)
    return render_template("global_summary.html",
                           matricule=matricule,
                           summary=global_summary)


@app.route("/<record_id>/delete", methods=["POST"])
def delete_project_time(record_id):
    get_db().delete_project_hours(record_id)
    return redirect(request.referrer, "302")
