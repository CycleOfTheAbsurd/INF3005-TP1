#!/usr/bin/env python
# -*- encoding: utf-8 -*-

from collections import defaultdict

import sqlite3
import calendar


class Database:
    def __init__(self):
        self.connection = None

    def get_connection(self):
        if self.connection is None:
            self.connection = sqlite3.connect('db/TP1.db')
        return self.connection

    def disconnect(self):
        if self.connection is not None:
            self.connection.close()
            self.connection = None

    def get_hours_for_day(self, matricule, date):
        cursor = self.get_connection().cursor()
        cursor.execute(("SELECT * FROM heures "
                        "WHERE matricule=? AND date_publication=?"),
                       (matricule, date))
        hours = [list(row) for row in cursor.fetchall()]
        # Convert minutes to hours for display
        for hour in hours:
            hour[4] = round(int(hour[4])/60, 2)
        return hours

    def delete_project_hours(self, record_id):
        print("Delete " + str(record_id))
        connection = self.get_connection()
        cursor = connection.cursor()
        cursor.execute("DELETE FROM heures WHERE id=?", (record_id,))
        connection.commit()

    def upsert_project_hours(self, matricule, project, date, hours):
        minutes = round(hours * 60, 2)
        connection = self.get_connection()
        cursor = connection.cursor()
        cursor.execute("""SELECT id FROM heures
                          WHERE matricule=? AND code_de_projet=?
                          AND date_publication=?""",
                       (matricule, project, date))
        try:
            record_id = cursor.fetchone()[0]
        except TypeError:
            cursor.execute("""INSERT INTO heures(matricule, code_de_projet, date_publication, duree)
                           VALUES(?,?,?,?)""",
                           (matricule, project, date, minutes))
        else:
            cursor.execute("UPDATE heures SET duree=? WHERE id=?",
                           (minutes, record_id))
        connection.commit()

    def get_monthly_summary(self, matricule, month):
        cursor = self.get_connection().cursor()
        cursor.execute("""SELECT * FROM heures WHERE matricule=?
                       AND date_publication LIKE ?""",
                       (matricule, month + "%"))

        # Builds a dict with basic month information
        first_day, num_days = calendar.monthrange(int(month[:4]),
                                                  int(month[5:]))
        month_info = {"num_days": num_days, "first_day": first_day}
        month_summary = {"{}-{:02d}".format(month, day): 0
                         for day in range(1, num_days+1)}

        try:
            for *_, date, minutes in cursor.fetchall():
                month_summary[date] += minutes
        except TypeError:
            return None
        else:
            month_summary = {date: round(minutes / 60, 2)
                             for date, minutes in month_summary.items()}
            month_info["summary"] = month_summary
            return month_info

    def get_general_summary(self, matricule):
        cursor = self.get_connection().cursor()
        cursor.execute("""SELECT * FROM heures WHERE matricule=?
                       ORDER BY date_publication""",
                       (matricule,))
        summary = defaultdict(int)
        try:
            for *_, date, minutes in cursor.fetchall():
                summary[date[:-3]] += round(minutes / 60, 2)
        except TypeError:
            return None
        else:
            return dict(summary)
