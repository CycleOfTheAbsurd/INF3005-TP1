#!/usr/bin/env python
# -*- encoding: utf-8 -*-
from datetime import datetime
from datetime import date
from datetime import timedelta
from dateutil.relativedelta import relativedelta


def get_prev_next_day(datestring):
    today = datetime.strptime(datestring, "%Y-%m-%d").date()
    yesterday = (today - timedelta(days=1)).isoformat()
    tomorrow = (today + timedelta(days=1)).isoformat()
    today = today.isoformat()
    return {"yesterday": yesterday, "today": today, "tomorrow": tomorrow}


def get_prev_next_month(datestring):
    month = datetime.strptime(datestring, "%Y-%m").date()
    last_month = (month - relativedelta(months=1)).strftime("%Y-%m")
    next_month = (month + relativedelta(months=1)).strftime("%Y-%m")
    month = month.strftime("%Y-%m")
    return {"last_month": last_month, "month": month, "next_month": next_month}
