PHONY: install run

install:
	pip3 install --user -r requirements.txt

run:
	FLASK_APP=index.py flask run
